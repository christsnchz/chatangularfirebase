import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/servicios/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styles: []
})
export class ChatComponent implements OnInit {

  mensaje:string = "";
  elemento:any;

  constructor(public _chatservice : ChatService) {

    this._chatservice.cargarMensajes().subscribe(
      ()=>{

        this.elemento.scrollTop = this.elemento.scrollHeight;
      }

    );

   }

  ngOnInit() {

    this.elemento = document.getElementById('app-mensajes');
     
  }

  enviar_mensaje(){

    if(this.mensaje.length === 0)
      return;

    this._chatservice.agregarMensaje(this.mensaje)
    .then(  ()=>{ this.mensaje='' } )
    .catch((err)=>{console.error('Error al enviar', err) } )

  }

}
