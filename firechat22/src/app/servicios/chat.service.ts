import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import {mensajeModel } from "../interfaces/mensaje.interface";
import { map } from 'rxjs/operators';

import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from  "firebase/app";

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private itemsCollection: AngularFirestoreCollection< mensajeModel >;

  public chats: mensajeModel[] = [];

  public usuario:any = {};

  constructor( private afs : AngularFirestore ,
               private afAuth : AngularFireAuth   ) { 

                this.afAuth.authState.subscribe( user => {
                  if(!user){
                    return;
                  }

                  this.usuario.nombre = user.displayName;
                  this.usuario.uid = user.uid;

                })

               }


  login(){
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }
  logout(){
    this.usuario = {};
    this.afAuth.auth.signOut();
  }



  cargarMensajes(){

    this.itemsCollection = this.afs.collection<mensajeModel>('chats', ref=> ref.orderBy('fecha', 'desc').limit(10));

    return this.itemsCollection.valueChanges()
    .pipe( map( (mensajes:mensajeModel[]) => {
      this.chats = [];

      for (let mensaje of mensajes){
        this.chats.unshift(mensaje);
      }

      return this.chats;

}))
               

  }

  agregarMensaje(texto:string){

    let mensaje:mensajeModel = {
      nombre: this.usuario.nombre,
      mensaje:texto,
      uid: this.usuario.uid,
      fecha: new Date().getTime()
    }

    return this.itemsCollection.add(mensaje)


  }


}
