// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAQkrnbgUvnEYzj99GXn3f1e4z8UbmJU8I",
    authDomain: "firechat-49250.firebaseapp.com",
    databaseURL: "https://firechat-49250.firebaseio.com",
    projectId: "firechat-49250",
    storageBucket: "firechat-49250.appspot.com",
    messagingSenderId: "667564330699",
    appId: "1:667564330699:web:9b266b58b2d3499ebb154e",
    measurementId: "G-1KMTC79WME"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
